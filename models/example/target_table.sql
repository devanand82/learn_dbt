-- depends_on: {{ ref('raw_join_transform') }}
{{ config(materialized='incremental', transient=false, unique_key='shakey_raw') }}

with raw_join_transform_cte as 
(
  select {{ dbt_utils.surrogate_key(['raw_join_transform.c_custkey','raw_join_transform.nation'])}} shakey_raw,
  raw_join_transform.* from {{ var('raw_join') }} 
)

select * from raw_join_transform_cte

{% if is_incremental() %}

  where greatest(r_loaddate,o_loaddate) > (select max(load_date) from {{this}})

{% endif %}
