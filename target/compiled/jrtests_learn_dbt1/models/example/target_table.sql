-- depends_on: analytics.dbt.raw_join_transform


with raw_join_transform_cte as 
(
  select md5(cast(coalesce(cast(raw_join_transform.c_custkey as 
    varchar
), '') || '-' || coalesce(cast(raw_join_transform.nation as 
    varchar
), '') as 
    varchar
)) shakey_raw,
  raw_join_transform.* from ['raw_join_transform', 'raw_join_transform2'] 
)

select * from raw_join_transform_cte



  where greatest(r_loaddate,o_loaddate) > (select max(load_date) from analytics.dbt.target_table)

