begin;
    
        
        
    

    

    merge into analytics.dbt.target_table as DBT_INTERNAL_DEST
        using analytics.dbt.target_table__dbt_tmp as DBT_INTERNAL_SOURCE
        on 
            DBT_INTERNAL_SOURCE.shakey_raw = DBT_INTERNAL_DEST.shakey_raw
        

    
    when matched then update set
        "SHAKEY_RAW" = DBT_INTERNAL_SOURCE."SHAKEY_RAW","C_CUSTKEY" = DBT_INTERNAL_SOURCE."C_CUSTKEY","C_NAME" = DBT_INTERNAL_SOURCE."C_NAME","NATION" = DBT_INTERNAL_SOURCE."NATION","R_LOADDATE" = DBT_INTERNAL_SOURCE."R_LOADDATE","O_LOADDATE" = DBT_INTERNAL_SOURCE."O_LOADDATE","TOTAL_ORDER_PRICE" = DBT_INTERNAL_SOURCE."TOTAL_ORDER_PRICE","LOAD_DATE" = DBT_INTERNAL_SOURCE."LOAD_DATE"
    

    when not matched then insert
        ("SHAKEY_RAW", "C_CUSTKEY", "C_NAME", "NATION", "R_LOADDATE", "O_LOADDATE", "TOTAL_ORDER_PRICE", "LOAD_DATE")
    values
        ("SHAKEY_RAW", "C_CUSTKEY", "C_NAME", "NATION", "R_LOADDATE", "O_LOADDATE", "TOTAL_ORDER_PRICE", "LOAD_DATE")

;
    commit;